#include <iostream>
#include <chrono>
#include <random>
using namespace std;

// construct a trivial random generator engine from a time-based seed:
unsigned seed = chrono::system_clock::now().time_since_epoch().count(); // without it we will get same numbers for each program run
std::default_random_engine generator(seed);
std::uniform_real_distribution<double> distribution(0.0, 1.0);

double getUniformRealDistribution()
{
	return distribution(generator);
}

double nr(double m, double d)
{
	double result = 0;
	for (int i = 0; i < 12; i++) 
	{
		result += getUniformRealDistribution();
	}
	return m + d * (result - 6);
}

double getChronometerError(int days)
{
	double result = 0;
	double last = 0;
	for (int i = 0; i < days; i++)
	{
		double currentNr = nr(0, 1);
		last = last + currentNr;
		result += last;
	}
	return result;
}

int main() 
{
	int errorsGreat = 0;
	for (int i = 0; i < 1000; i++)
	{
		double h1 = getChronometerError(7);
		double h2 = getChronometerError(7);
		double h3 = getChronometerError(7);

		double midError = (h1 + h2 + h3) / 3;
		//cout << midError << endl;
		if (abs(midError) > 2)
		{
			errorsGreat += 1;
		}
	}

	cout << "error probability > 2 min " << errorsGreat/1000.0 << endl;

	int stop;
	cin >> stop; // stop console

	return 0;
}