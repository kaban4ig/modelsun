#include <iostream>
#include <chrono>
#include <random>
using namespace std;

// construct a trivial random generator engine from a time-based seed:
unsigned seed = chrono::system_clock::now().time_since_epoch().count(); // without it we will get same numbers for each program run
std::default_random_engine generator(seed);
std::uniform_real_distribution<double> distribution(0.0, 1.0);

double getUniformRealDistribution()
{
	return distribution(generator);
}

int main()
{
	

	double sideSquare = 2;
	double radius = sideSquare / 2;

	int dots[6] = { 30, 60, 100, 1000, 10000, 1000000 };

	for (int i = 0; i < 6; i++) 
	{
		int countDots = 0;
		
		for (int j = 0; j < dots[i]; j++)
		{
			double x = sideSquare * getUniformRealDistribution();
			double y = sideSquare * getUniformRealDistribution();
			
			if (sqrt(pow(radius - x, 2) + pow(radius - y, 2)) < radius) // i use radius instead of center of square because it same for square
			{
				countDots += 1;
			}
		}
		cout << "count of dots in the circle for " << dots[i] << " dots: " << countDots << endl;
		cout << "The area of the inscribed circle: " << (double)countDots / dots[i] * (sideSquare * sideSquare) << endl;
	}

	int stop;
	cin >> stop; // without it, console will closed

	return 0;
}

