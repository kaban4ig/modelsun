#include <iostream>
#include <Windows.h>
#include <stdlib.h>
#include <math.h>
using namespace std;

int main()
{

	//for graphics
	HDC hDC = GetDC(GetConsoleWindow());
	HPEN Pen = CreatePen(PS_SOLID, 2, RGB(255, 255, 255));
	SelectObject(hDC, Pen);
	MoveToEx(hDC, 100, 100, NULL);
	LineTo(hDC, 200, 100);
	MoveToEx(hDC, 100, 100, NULL);
	LineTo(hDC, 100, 200);
	//end for graphic

	int  n;
	double x, dT = 0.01, t = 0, k = 0.01;
	cout << "recomended for good graphics: x = 5, n = 200" << endl;
	cout << "x: ";
	cin >> x;
	cout << "n: ";
	cin >> n;
	while (x < 0.95 * n)
	{
		x = x + dT * k * x * (n - x);
		t = t + dT;
		
		MoveToEx(hDC, 100 + x, 100 + t * 10, NULL); //*10 for good graphic
		LineTo(hDC, 100 + x, 100 + t * 10);
	}
	cout << "t: " << t << " x: " << x << endl;

	int stop;
	cin >> stop;

	return 0;
}