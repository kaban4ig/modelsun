#include <iostream>
#include <chrono>
#include <random>
using namespace std;

// construct a trivial random generator engine from a time-based seed:
unsigned seed = chrono::system_clock::now().time_since_epoch().count(); // without it we will get same numbers for each program run
std::default_random_engine generator(seed);
std::uniform_real_distribution<double> distribution(0.0, 1.0);

double getUniformRealDistribution()
{
	return distribution(generator);
}

int main()
{

	const int castCount = 500, testCount = 10, cubeFaces = 6;

	double probabilityStep = 1. / cubeFaces;

	for (int i = 0; i < testCount; i++)
	{
		cout << "expected value of test " << i + 1 << ": ";
		double expectedValue = 0;
		for (int j = 0; j < castCount; j++)
		{
			double n = getUniformRealDistribution();
			int k = 1;
			while (n - k*probabilityStep > 0)
			{
				k += 1;
			}
			expectedValue += k;
		}
		cout << expectedValue/(double)castCount << endl;
	}

	int stop;
	cin >> stop; // without it, console will closed

	return 0;
}

