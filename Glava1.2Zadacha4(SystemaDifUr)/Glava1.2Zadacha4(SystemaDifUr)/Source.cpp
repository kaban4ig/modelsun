#include <iostream>
#include <Windows.h>
#include <stdlib.h>
#include <math.h>
using namespace std;

void draw(HDC hDC, double x, double y, double a1, double a2, double t, double dT, int shiftX, int shiftY)
{
	int n = 100, scale = 100, posX, posY;
	double lastX = x, lastY = y;
	while (x<2 && y<2 && x>=0 && y>=0)
	{
		lastX = x + dT * (a1*x + x*x*y + x*x*x*y + x*x*x*x*y);
		lastY = y + dT * (a2*y + x*y*y + x*y*y*y + x*y*y*y*y);
		x = lastX;
		y = lastY;

		//cout << "t: " << t << " x: " << x << " y: " << y << endl;
		t = t + dT;

		posX = x*scale;
		posY = y*scale;
		MoveToEx(hDC, shiftX + posX, shiftY + posY, NULL);
		LineTo(hDC, shiftX + posX, shiftY + posY);
	}
}

int main()
{

	//for graphic
	HDC hDC = GetDC(GetConsoleWindow());
	HPEN Pen = CreatePen(PS_SOLID, 2, RGB(255, 255, 255));
	SelectObject(hDC, Pen);
	MoveToEx(hDC, 100, 100, NULL);
	LineTo(hDC, 300, 100);
	MoveToEx(hDC, 100, 100, NULL);
	LineTo(hDC, 100, 300);
	//end for graphic

	double a1, a2, x, y, dT = 0.000001, t = 0;
	//cout << "x: ";
	//cin >> x;
	//cout << "y: ";
	//cin >> y;

	//cout << endl << "a2 < a1 < 0" << endl;
	//cout << "a1: ";
	//cin >> a1;
	//cout << "a2: ";
	//cin >> a2;

	//1
	x = 1.3;
	y = 1;
	a1 = -0.000002;
	a2 = -0.0000021;
	draw(hDC, x, y, a1, a2, t, dT, 0, 40);

	//2
	x = 1;
	y = 1;
	a1 = -0.000002;
	a2 = -4.9;
	draw(hDC, x, y, a1, a2, t, dT, 0, 100);

	//3
	x = 1;
	y = 1;
	a1 = -3.05;
	a2 = -3.05;
	//draw(hDC, x, y, a1, a2, t, dT, 200, 100);


	int stop;
	cin >> stop;

	return 0;
}