#include <iostream>
#include <chrono>
#include <random>
#include <math.h>
using namespace std;

// construct a trivial random generator engine from a time-based seed:
unsigned seed = chrono::system_clock::now().time_since_epoch().count(); // without it we will get same numbers for each program run
std::default_random_engine generator(seed);
std::uniform_real_distribution<double> distribution(0.0, 1.0);

double getUniformRealDistribution()
{
	return distribution(generator);
}

double getExponentialDistribution(double l) 
{
	return ((-1) / l) * log(getUniformRealDistribution());
}

int main()
{
	double h, p, l = 1;
	int countInH = 0;

	cout << "end point: ";
	cin >> h;

	cout << endl << "stop propability ";
	cin >> p;

	cout << endl;

	for (int i = 0; i < 1000; i++)
	{
		double currentPosition = 0;
		bool isStop = false;

		while (currentPosition < h && !isStop)
		{
			currentPosition += getExponentialDistribution(l);
			if (getUniformRealDistribution() <= p) 
			{
				isStop = true;
			}
		}

		if (currentPosition >= h) 
		{
			countInH += 1;
		}
	}

	cout << "probability of reaching a point " << countInH / 1000.0 << endl;

	int stop;
	cin >> stop; // stop console

	return 0;
}